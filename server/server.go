package server

import (
	t "lpc/types"
)

func New(lfp t.LoggerForPackage, other ...interface{}) t.ServerX {
	type dummy struct{} // don't refactor it to some utils!
	logger := lfp(dummy{})
	//...other initialization
	_ = other
	return &Server{logger}
}

// TODO gracefully handle closed channel/termination
func CNew(ls *t.LoggerSupport, other ...interface{}) t.ServerX {
	for {
		select {
		case logger := <-ls.Logger: // this will block until package is figured out and logger instance sent
			server := &Server{logger}
			//...other initialization
			_ = other
			return server
		default:
			type dummy struct{} // Must be locally defined, don't refactor it to some utils!
			ls.Pkg <- dummy{}   // the caller will figure out a package for local type dummy
		}
	}
}

type Server struct {
	logger *t.Logger
	//other stuff
}

func (s *Server) Logger() *t.Logger {
	return s.logger
}

func (s *Server) Serve() error {
	return nil
}
