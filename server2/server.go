package server2

import (
	t "lpc/types"
)

func New(lfp t.LoggerForPackage, other ...interface{}) t.ServerX {
	type dummy struct{} // don't refactor it to some utils!
	logger := lfp(dummy{})
	//...other initialization
	_ = other
	return &Server2{logger}
}

type Server2 struct {
	logger *t.Logger
	//other stuff
}

func (s *Server2) Logger() *t.Logger {
	return s.logger
}

func (s *Server2) Serve() error {
	return nil
}
