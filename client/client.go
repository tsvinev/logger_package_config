package client

import (
	s "lpc/server"
	s2 "lpc/server2"
	t "lpc/types"
	u "lpc/util"
)

func ConstructorUser() (t.ServerX, t.ServerX) {

	pn := t.LoggerForPackage(func(t interface{}) *t.Logger { return u.LoggerForPackage(u.PackageForType(t)) })

	myObject := s.New(pn, 1, 2, 3)
	myObject2 := s2.New(pn, 1, 2, 3)

	return myObject, myObject2
}

// The idea is to use the same channels for constructors for all types
// as long as they all comply to the protocol of sending a dummy, locally defined
// struct instance as a carrier of type, which includes package,
// and receive a properly configured logger for that package
//
func ConstructorChanUser() (t.ServerX, t.ServerX) {

	ls := &t.LoggerSupport{
		Pkg:    make(chan interface{}),
		Logger: make(chan *t.Logger),
	}
	go getLoggerForPackage(ls)

	myObject := s.CNew(ls, 1, 2, 3)

	pn := t.LoggerForPackage(func(t interface{}) *t.Logger { return u.LoggerForPackage(u.PackageForType(t)) })
	myObject2 := s2.New(pn, 1, 2, 3)

	return myObject, myObject2
}

// Here we loop forever, receving a package on Pkg channel
// and sending a configured logger in response, to Logger channel
// TODO gracefully handle closed channel/termination
func getLoggerForPackage(ls *t.LoggerSupport) {
	for {
		select {
		case t := <-ls.Pkg:
			ls.Logger <- u.LoggerForPackage(u.PackageForType(t))
		}
	}
}
