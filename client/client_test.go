package client

import "testing"
import "github.com/stretchr/testify/assert"

func TestServer1(t *testing.T) {
	o, _ := ConstructorUser()
	assert.Equal(t, 1, o.Logger().Level)
	assert.Equal(t, "lpc/server", o.Logger().Pkg)
}

func TestServer2(t *testing.T) {
	_, o := ConstructorUser()
	assert.Equal(t, 3, o.Logger().Level)
	assert.Equal(t, "lpc/server2", o.Logger().Pkg)
}
