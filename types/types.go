package types

type Logger struct {
	Pkg   string
	Level int
}

type LoggerSupport struct {
	Pkg    chan interface{}
	Logger chan *Logger
}

type LoggerForPackage func(t interface{}) *Logger

type ServerX interface {
	Serve() error
	Logger() *Logger
}
