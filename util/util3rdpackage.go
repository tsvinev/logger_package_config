package util

import (
	t "lpc/types"
	"reflect"
)

// Fakes different log levels for different packages
// returns default logger in case of error, never an error
func LoggerForPackage(packageName string) *t.Logger {
	level := 42
	switch packageName {
	case "lpc/server":
		level = 1
	case "lpc/server2":
		level = 3
	}
	return &t.Logger{Pkg: packageName, Level: level}
}

func PackageForType(t interface{}) string {
	return reflect.TypeOf(t).PkgPath()
}
